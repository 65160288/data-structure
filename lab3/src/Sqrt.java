public class Sqrt {

    static int binary(int x){
        if(x==0 || x==1){
            return x;
        }
        int left = 1,right = x;
        while(left <= right){
            int mid = left + (right - left) / 2;

            if(mid == x / mid){
                return mid;
            }else if (mid > x / mid) {
                right = mid - 1;
            }else{
                left = mid + 1;
            }
        }
        return right;
    }

    public static void main(String[] args) {
        System.out.println("Output: " + binary(25));
    }
}
