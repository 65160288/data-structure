import java.util.Scanner;

public class ArrayDeleteionalLab{
    static Scanner kb = new Scanner(System.in);
    static int[] arr = {1,2,3,4,5};
    
    static int[] deleteElementByIndex(int[] arr,int index){
        int[] newArray = new int[arr.length-1];
        int j = 0;
        for(int i = 0;i < arr.length;i++){
            if(i != index){
                newArray[j] = arr[i];
                j++;
            }
        }

        for(int i = 0;i < newArray.length;i++){
            System.out.print(newArray[i]+" ");
        }
        System.out.println();
        return newArray;
    }
// static void deleteElementByValue(int[] newArray,int value){
//     int[] newArray1 = new int[newArray.length-1];
//     int deleteValue = value;
//     int newIndex = 0;
//     for(int i = 0;i <newArray.length;i++){
//         if(newArray1[i] == deleteValue){
//             i++;
//         }
//         newArray1[newIndex] = newArray1[i];
//             newIndex++;
//     }
//
//     for(int i = 0; i < newArray1.length;i++){
//         System.out.println(newArray1[i]+" ");
//     }
// }
    public static int[] deleteElementByValue(int[] arr, int value) {
        int[] newArray1 = new int[arr.length-1];
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (newArray1[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return newArray1; // Value not found, return the original array
        }

        return deleteElementByIndex(arr, index);
    }
    public static void main(String[] args) {
        int index = kb.nextInt();
        int value = kb.nextInt();
        deleteElementByIndex(arr,index);
        deleteElementByValue(arr,value);
    }
}